
public class Main {


	public static void main(String[] args) {
		
		//first room 
		/**
		 * Showing the first room with getWalls.
		 */
		Room first = new Room("hardwood","yellow", 1);
		first.getWalls();
		
		System.out.println(first);
		first.setFloors("hardwood");
		first.setWindows (1);
		
		/**
		 * Showing the second room with getFloors
		 */
		
		//second room
		Room second = new Room ("tiled","purple", 0);
		second.getFloors();
		System.out.println(second);
		first.setFloors("tiled");
		first.setWindows(0);
		/**
		 * Showing the third room with getWindows.
		 */
		
		// third room
		Room third = new Room("carpeted","white", 3);
		third.getWindows();
		System.out.println(third);
		first.setFloors("carpet");
		first.setWindows(3);
		
		
	

	

	}

}
